from framework.client.api_client import ApiClient
from framework.client.routes import Routes


def get_objects(client: ApiClient, *ids):
    return client.get(Routes.OBJECTS, params={'id': ids} if ids else None)


def get_object(client: ApiClient, obj_id: str):
    return client.get(Routes.OBJECTS_ITEM.format(obj_id))


def post_object(client: ApiClient, **kwargs):
    return client.post(Routes.OBJECTS, **kwargs)


def put_object(client: ApiClient, obj_id: str, **kwargs):
    return client.put(Routes.OBJECTS_ITEM.format(obj_id), **kwargs)


def delete_object(client: ApiClient, obj_id: str, **kwargs):
    return client.delete(Routes.OBJECTS_ITEM.format(obj_id))